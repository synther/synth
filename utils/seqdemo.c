#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <alsa/asoundlib.h>

snd_seq_t *open_seq();
void setup_seq_port_subscribe(snd_seq_port_subscribe_t *subs);

snd_seq_t *open_seq() {

  snd_seq_t *seq_handle;
  int portid;

  if (snd_seq_open(&seq_handle, "default", SND_SEQ_OPEN_INPUT, 0) < 0) {
    fprintf(stderr, "Error opening ALSA sequencer.\n");
    exit(1);
  }
  snd_seq_set_client_name(seq_handle, "ALSA Sequencer Demo");
  if ((portid = snd_seq_create_simple_port(seq_handle, "ALSA Sequencer Demo Port",
            SND_SEQ_PORT_CAP_WRITE|SND_SEQ_PORT_CAP_SUBS_WRITE,
            SND_SEQ_PORT_TYPE_MIDI_GENERIC|SND_SEQ_PORT_TYPE_APPLICATION)) < 0) {
    fprintf(stderr, "Error creating sequencer port.\n");
    exit(1);
  }
  return(seq_handle);
}

void setup_seq_port_subscribe(snd_seq_port_subscribe_t *subs) {
  snd_seq_addr_t sender;
  sender.client = 20;
  sender.port = 0;
  snd_seq_port_subscribe_set_sender(subs, &sender);


  snd_seq_addr_t dest;
  dest.client = 128;
  dest.port = 0;
  snd_seq_port_subscribe_set_dest(subs, &dest);
  snd_seq_port_subscribe_set_queue(subs, 1);
  snd_seq_port_subscribe_set_time_real(subs, 1);
  snd_seq_port_subscribe_set_time_update(subs, 1);
}


int main(int argc, char *argv[]) {

  snd_seq_t *seq_handle;
  seq_handle = open_seq();

  snd_seq_port_subscribe_t *subs;
  snd_seq_port_subscribe_malloc(&subs);
  setup_seq_port_subscribe(subs);
  snd_seq_subscribe_port(seq_handle, subs);

  snd_seq_event_t *ev;
  while (snd_seq_event_input(seq_handle, &ev) >= 0) {

    switch (ev->type) {
      case SND_SEQ_EVENT_CONTROLLER: 
        printf("Control change        channel %2d, controller %d, value %d\n",
          ev->data.control.channel, ev->data.control.param, ev->data.control.value);
        break;
      case SND_SEQ_EVENT_NOTEON:
        if (ev->data.note.velocity) 
          printf("Note on               channel %2d, note %d, velocity %d\n",
            ev->data.note.channel, ev->data.note.note, ev->data.note.velocity);
         else 
          printf("Note off              channel %2d, note %d\n",
            ev->data.note.channel, ev->data.note.note);
        break;
      case SND_SEQ_EVENT_NOTEOFF: 
        printf("Note off              channel %2d, note %d, velocity %d\n",
          ev->data.note.channel, ev->data.note.note, ev->data.note.velocity);
        break;        

    }
    snd_seq_free_event(ev);
  }

}
#include "qtquickcontrolsapplication.h"
#include <QtQml>
#include <QtWidgets>
#include "params.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    QQmlEngine engine;
    Params params;

    engine.rootContext()->setContextProperty("params", &params);
    QQmlComponent component(&engine, QUrl("qrc:/main.qml"));
    component.create();
    return app.exec();
}

#ifndef PARAMS_H
#define PARAMS_H
#include <QtCore>
#include <QDebug>

class Params : public QObject {
    Q_OBJECT
    Q_PROPERTY(QString harmonic READ harmonic WRITE setHarmonic NOTIFY harmonicChanged)
    Q_PROPERTY(QString subharmonic READ subharmonic WRITE setSubharmonic NOTIFY subharmonicChanged)
    Q_PROPERTY(QString transpose READ transpose WRITE setTranspose NOTIFY transposeChanged)
    Q_PROPERTY(QString pitch READ pitch WRITE setPitch NOTIFY pitchChanged)
    Q_PROPERTY(QString modulation READ modulation WRITE setModulation NOTIFY modulationChanged)
    Q_PROPERTY(QString attack READ attack WRITE setAttack NOTIFY attackChanged)
    Q_PROPERTY(QString decay READ decay WRITE setDecay NOTIFY decayChanged)
    Q_PROPERTY(QString sustain READ sustain WRITE setSustain NOTIFY sustainChanged)
    Q_PROPERTY(QString release READ release WRITE setRelease NOTIFY releaseChanged)

public:
    void setHarmonic(const QString &s);
    void setSubharmonic(const QString &s);
    void setTranspose(const QString &s);    
    void setPitch(const QString &s);
    void setModulation(const QString &s);
    void setAttack(const QString &s);
    void setDecay(const QString &s);
    void setSustain(const QString &s);
    void setRelease(const QString &s);

    QString harmonic() const {return p_harmonic;}
    QString subharmonic() const {return p_subharmonic;}
    QString transpose() const {return p_transpose;}
    QString pitch() const {return p_pitch;}
    QString modulation() const {return p_modulation;}
    QString attack() const {return p_attack;}
    QString decay() const {return p_decay;}
    QString sustain() const {return p_sustain;}
    QString release() const {return p_release;}

signals:
    void harmonicChanged();
    void subharmonicChanged();
    void transposeChanged();    
    void pitchChanged();
    void modulationChanged();
    void attackChanged();
    void decayChanged();
    void sustainChanged();
    void releaseChanged();


private:
    QString p_harmonic, p_subharmonic, p_transpose;
    QString p_pitch, p_modulation, p_attack, p_decay, p_sustain, p_release;

};

#endif // PARAMS_H

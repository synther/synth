#include "params.h"

void Params::setHarmonic(const QString &s) {
    qDebug() << "setHarmonic" << s;
    if ("" != s && s != p_harmonic) {
        p_harmonic = s;
        emit harmonicChanged();
    }
}

void Params::setSubharmonic(const QString &s) {
    qDebug() << "setSubharmonic" << s;
    if ("" != s && s != p_subharmonic) {
        p_subharmonic = s;
        emit subharmonicChanged();
    }
}

void Params::setTranspose(const QString &s) {
    qDebug() << "setTranspose" << s;
    if ("" != s && s != p_transpose) {
        p_transpose = s;
        emit transposeChanged();
    }
}

void Params::setPitch(const QString &s) {
    qDebug() << "setPitch " << s;
    if ("" != s && s != p_pitch) {
        p_pitch = s;
        emit pitchChanged();
    }
}

void Params::setModulation(const QString &s) {
    qDebug() << "setModulation " << s;
    if ("" != s && s != p_modulation) {
        p_modulation = s;
        emit modulationChanged();
    }
}

void Params::setAttack(const QString &s) {
    qDebug() << "setAttack " << s;
    if ("" != s && s != p_attack) {
        p_attack = s;
        emit attackChanged();
    }
}

void Params::setDecay(const QString &s) {
    qDebug() << "setDecay " << s;
    if ("" != s && s != p_decay) {
        p_decay = s;
        emit decayChanged();
    }
}

void Params::setSustain(const QString &s) {
    qDebug() << "setSustain " << s;
    if ("" != s && s != p_sustain) {
        p_sustain = s;
        emit sustainChanged();
    }
}

void Params::setRelease(const QString &s) {
    qDebug() << "setRelease " << s;
    if ("" != s && s != p_release) {
        p_release = s;
        emit releaseChanged();
    }
}

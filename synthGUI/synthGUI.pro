QT += qml quick
TARGET = synthGUI
!android: !ios: !blackberry: qtHaveModule(widgets): QT += widgets

include(src/src.pri)

OTHER_FILES += \
    main.qml
#    content/AboutDialog.qml \
#    content/ChildWindow.qml \

RESOURCES += \
    resources.qrc


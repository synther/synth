
import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Layouts 1.0
import QtQuick.Dialogs 1.0
import "content"

ApplicationWindow {
    visible: true
    title: "Synthesizer"

    width: 640
    height: 420
    minimumHeight: 400
    minimumWidth: 600

    SystemPalette {id: syspal}
    color: syspal.window

    ColumnLayout {

        id: paramsLayout

        RowLayout {
            id: harmonicLayout
            Label {
                text: "Harmonic"
            }
            TextField {
                id: harmonicField
                validator: IntValidator {bottom: 0; top: 255;}
                text: params.harmonic
            }
        }

        RowLayout {
            id: subharmonicLayout
            Label {
                text: "Subharmonic"
            }
            TextField {
                id: subharmonicField
                text: params.subharmonic
                validator: IntValidator {bottom: 0; top: 255;}
            }
        }

        RowLayout {
            id: transposeLayout
            Label {
                text: "Transpose"
            }
            TextField {
                id: transposeField
                validator: IntValidator {bottom: 0; top: 255;}
                text: params.transpose
            }
        }

        RowLayout {
            id: pitchLayout
            Label {
                text: "Pitch"
            }
            TextField {
                id: pitchField
                validator: IntValidator {bottom: 0; top: 255;}
                text: params.pitch
            }
        }

        RowLayout {
            id: modulationLayout
            Label {
                text: "Modulation"
            }
            TextField {
                id: modulationField
                validator: IntValidator {bottom: 0; top: 255;}
                text: params.modulation
            }
        }

        RowLayout {
            id: attackLayout
            Label {
                text: "Attack"
            }
            TextField {
                id: attackField
                validator: IntValidator {bottom: 0; top: 255;}
                text: params.attack
            }
        }

        RowLayout {
            id: decayLayout
            Label {
                text: "Decay"
            }
            TextField {
                id: decayField
                validator: IntValidator {bottom: 0; top: 255;}
                text: params.decay
            }
        }


        RowLayout {
            id: sustainLayout
            Label {
                text: "Sustain"
            }
            TextField {
                id: sustainField
                validator: IntValidator {bottom: 0; top: 255;}
                text: params.sustain
            }
        }


        RowLayout {
            id: releaseLayout
            Label {
                text: "Release"
            }
            TextField {
                id: releaseField
                validator: IntValidator {bottom: 0; top: 255;}
                text: params.release
            }
        }


        Button {
            id: submitButton
            text: "Submit"
            onClicked: {
                params.harmonic = harmonicField.text
                params.subharmonic = subharmonicField.text
                params.transpose = transposeField.text
                params.pitch = pitchField.text
                params.modulation = modulationField.text
                params.attack = attackField.text
                params.decay = decayField.text
                params.sustain = sustainField.text
                params.release = releaseField.text

           }
        }


    }

}

